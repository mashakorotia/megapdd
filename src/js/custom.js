$('.card-slider').slick({
  slidesToScroll: 3,
  variableWidth: true,
  responsive: [
  {
    breakpoint: 1200,
    settings: {
      slidesToScroll: 1,
      centerMode: true,
    }
  },
  {
    breakpoint: 768,
    settings: {
      slidesToScroll: 2,
      centerMode: true,
    }
  },
  {
    breakpoint: 576,
    settings: {
      slidesToScroll: 1,
      centerMode: true,
    }
  }
  ]
});


if($('.sly')) {
  var $sly = $('.sly');
  var $nextArrow = $('.sly').parent().find('.sly-arrow');

  new Sly($sly, {
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: true,
    touchDragging: true,
    scrollTrap: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBy: 1,
    speed: 1,
    clickBar: 1,
    scrollBar: $sly.parent().find('.scrollbar'),
    dragHandle: 1,
    dynamicHandle: 1,
    next: $nextArrow,

  });
}


$('.select').select2({
  minimumResultsForSearch: -1,
});


$('.header-btn').click(function(){
  $('.header-menu').addClass('show');
  $('body').addClass('noscroll');
  $('.header-log-in').hide();
  $('.header-close-menu').show();
  $('.header-lk').hide();
});

$('.header-close-menu').click(function(){
  $('.header-menu').removeClass('show');
  $('body').removeClass('noscroll');
  $('.header-log-in').show();
  $('.header-close-menu').hide();
  $('.header-lk').show();
})


$('.js-password').click(function(){
  $('.new-password').addClass('show');
})

$('.js-wrong-btn').click(function(){
  if($(this).attr('data-open') == 'close'){
    $(this).parent().parent().find('.wrong-block').addClass('show');
    $(this).attr('data-open', 'open');
    $(this).html('Скрыть');
  } else {
    $(this).parent().parent().find('.wrong-block').removeClass('show');
    $(this).attr('data-open', 'close');
    $(this).html('Показать');
  }
})


/*tabs*/
$('.tab-nav .tab-nav__item').click(function(){
  var activeTab = $(this).parent().find('.tab-nav__item.active');
  var activeTabId = activeTab.attr('data-tab');
  var idTab = $(this).attr('data-tab');

  if (!($(this).hasClass('active'))) {
    activeTab.removeClass('active');
    $(this).addClass('active');
    $('#'+activeTabId).removeClass('show');
    $('#'+idTab).addClass('show');
  }
});

/*modal*/
$('.modal .modal-close').click(function(){
  $(this).closest('div.modal').removeClass('show');
});

/*course page*/
$('.course__list .course__item').click(function(){
  if (!($(this).hasClass('active'))) {
    $(this).parent().find('.course__item.active').removeClass('active');
    $(this).addClass('active');
  }
});


/*course-list mob*/
$('.btn-dots').click(function(){
  if($(this).attr('data-open') == 'close') {
    $(this).attr('data-open', 'open');
    $('.course__list').addClass('show');
  }
});

$('.course__list .course__list-close').click(function(){
  console.log('1');
  $('.btn-dots').attr('data-open', 'close');
  $(this).parent().parent().find('.course__list.show').removeClass('show');
})